<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateChuyenMucRequest;
use App\Http\Requests\DeleteChuyenMucRequest;
use App\Models\ChuyenMuc;
use Illuminate\Http\Request;

class ChuyenMucController extends Controller
{
    public function index()
    {
        toastr()->success("Hello everyone, welcome to my website.");
        return view('admin_lte.pages.chuyen_muc.index_chuyenmuc');
    }

    public function data()
    {
        $chuyenMuc = ChuyenMuc::get();
        return response()->json([
            'bede'  => $chuyenMuc,
        ]);
    }

    public function create(CreateChuyenMucRequest $request)
    {
        ChuyenMuc::create([
            'ten_chuyen_muc'     =>$request->ten_chuyen_muc,
            'slug_chuyen_muc'    =>$request->slug_chuyen_muc,
            'is_open'            =>$request->is_open,
        ]);

        return response()->json([
            'status' => true,
            'messa'   => 'Đã thêm sản phẩm thành công',
        ]);
    }

    public function delete_chuyenmuc (DeleteChuyenMucRequest $request)
    {
        ChuyenMuc::where('id', $request->id)->first()->delete();
        return response()->json([
            'status' => 0,
            'mess' => 'Đã xoá sản phẩm thành công',
        ]);
    }
}
