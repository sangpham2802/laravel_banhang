<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateLoaiSanPhamRequest;
use App\Http\Requests\DeleteLoaiSanPhamRequest;
use App\Http\Requests\UpdateLoaiSanPhamRequest;
use App\Models\LoaiSanPham;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class LoaiSanPhamController extends Controller
{
    public function index_lte ()
    {
        $danhmuccha = LoaiSanPham::where('id_loai_san_pham_cha',0)->get();
        toastr()->success("Hello everyone, welcome to my website.");



        return view('admin_lte.pages.loai_san_pham.index', compact('danhmuccha'));
    }

    public function getData ()
    {
        $sql = "SELECT A.TEN_LOAI_SAN_PHAM AS TEN_LOAI_CHA, B.*
            FROM LOAI_SAN_PHAMS A RIGHT JOIN LOAI_SAN_PHAMS B ON A.ID = B.ID_LOAI_SAN_PHAM_CHA";

        $danhmuc = DB::select($sql);

        return response()->json([
        'list'  => $danhmuc,
        ]);
    }


    public function create (CreateLoaiSanPhamRequest $request)
    {
        LoaiSanPham::create([
            'ten_loai_san_pham'     =>$request->ten_loai_san_pham,
            'slug_loai_san_pham'    =>$request->slug_loai_san_pham,
            'is_open'               =>$request->is_open,
            'id_loai_san_pham_cha'  =>$request->id_loai_san_pham_cha,
        ]);

        return response()->json([
            'status' => true,
            'mess'   => 'Đã thêm sản phẩm thành công',
        ]);
    }

    public function delete_new (DeleteLoaiSanPhamRequest $request)
    {
        LoaiSanPham::where('id', $request->id)->first()->delete();

        return response()->json([
            'status'  => 0,
            'mess' => 'Đã xoá thành công',
        ]);
    }
    public function update (UpdateLoaiSanPhamRequest $request)
    {
        $loaiSP = LoaiSanPham::where('id', $request->id)->first();
        $loaiSP->ten_loai_san_pham    = $request->ten_loai_san_pham;
        $loaiSP->slug_loai_san_pham    = $request->slug_loai_san_pham;
        $loaiSP->is_open    = $request->is_open;
        $loaiSP->id_loai_san_pham_cha    = $request->id_loai_san_pham_cha;
        $loaiSP->save();
        return response()->json([
            'status'  => 0,
            'mess' => 'Đã câp thành công',
        ]);
    }
    public function edit(DeleteLoaiSanPhamRequest $request){
        $loaiSP = LoaiSanPham::where('id', $request->id)->first();
        return response()->json([
            'data' => $loaiSP,
        ]);
    }

}
