<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateChuyenMucRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'ten_chuyen_muc'    =>'required|min:3|max:30',
            'slug_chuyen_muc'   =>'required|min:3|max:30',
            'is_open'           =>'required|boolean',
        ];
    }

    public function messages()
    {
        return [
            'ten_chuyen_muc.*'    =>'Tên chuyên mục phải có độ dài ký tự từ 3 đến 30 ký tự',
            'slug_chuyen_muc.*'   =>'Slug chuyên mục phải có độ dài ký tự từ 3 đến 30 ký tự',
            'is_open.*'           =>'Tình trạng yêu cầu phải chọn đúng',
        ];
    }
}
