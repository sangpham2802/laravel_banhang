<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateSanPhamRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'ten_san_pham'          => 'required|min:4|max:50',
            'slug_san_pham'         => 'required|min:4|max:50',
            'hinh_anh'              => 'required',
            'mo_ta_ngan'            => 'required',
            'mo_ta_chi_tiet'        => 'required',
            'is_open'               => 'required|boolean',
            'gia_ban'               => 'required|numeric|min:0',
            'gia_khuyen_mai'        => 'required|numeric|min:0|lte:gia_ban',
            'id_loai_san_pham'           => 'required|exists:loai_san_phams,id',
        ];
    }
    public function messages()
    {
        return [
            'ten_san_pham.*'          => 'Tên sản phẩm từ 4 đến 50 ký tự',
            'slug_san_pham.*'         => 'Slug sản phẩm từ 4 đến 50 ký tự',
            'hinh_anh.*'              => 'Hình ảnh không được bỏ trống',
            'mo_ta_ngan.*'            => 'Mô tả ngắn không được bỏ trống',
            'mo_ta_chi_tiet.*'        => 'Mô tả chi tiết không được bỏ trống',
            'is_open.*'               => 'Bạn phải chọn trình trạng',
            'gia_ban.*'               => 'Giá bán phải từ 0đ',
            'gia_khuyen_mai.*'        => 'Giá khuyến mãi phải nhỏ hơn hoặc bằng giá bán',
            'id_loai_san_pham.*'           => 'Loại sản phẩm không tồn tại',
        ];
    }
}
