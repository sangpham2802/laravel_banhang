<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateLoaiSanPhamRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'id'                    => 'required|exists:loai_san_phams,id',
            'ten_loai_san_pham'     => 'required|min:3|max:40',
            'slug_loai_san_pham'    => 'required|min:3|max:40',
            'is_open'               => 'required|boolean',
            'id_loai_san_pham_cha'  => 'required',
        ];
    }

    public function messages()
    {
        return[
            'id.*'                    => 'loại sản phẩm không tồn tại',
            'ten_loai_san_pham.*'     => 'Tên loại sản phẩm phải từ 3 đến 40 ký tự',
            'slug_loai_san_pham.*'    => 'Slug loại sản phẩm phải từ 3 đến 40 ký tự',
            'is_open.*'               => 'Tình trạng yêu cầu phải chọn đúng',
            'id_loai_san_pham_cha.*'  => 'Loại sản phẩm cha không được để trống',
        ];
    }
}
