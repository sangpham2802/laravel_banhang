var xxx = {

};
axios
    .post('', xxx)
    .then((res) => {
        if(res.data.status) {
            toastr.success(res.data.mess);
        } else if(toastr.data.status == 0) {
            toastr.error(res.data.mess);
        } else if(toastr.data.status == 2) {
            toastr.warning(res.data.mess);
        }
    })
    .catch((res) => {
        var listE = res.response.data.errors;
        $.each(listE, function(k, v) {
            toastr.error(v[0]);
        });
    });
