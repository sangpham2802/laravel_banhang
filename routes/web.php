<?php

use App\Http\Controllers\ChuyenMucController;
use App\Http\Controllers\TestController;
use App\Http\Controllers\LoaiSanPhamController;
use App\Http\Controllers\SanPhamController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/test',[TestController::class, 'index']);

Route::group(['prefix' => '/admin-lte'], function() {
    Route::group(['prefix' => '/loai-san-pham'], function() {
        Route::get('/index-lte', [LoaiSanPhamController::class, 'index_lte']);
        Route::get('/data',[LoaiSanPhamController::class, 'getData']);
        Route::post('/create',[LoaiSanPhamController::class, 'create']);
        Route::post('/delete',[LoaiSanPhamController::class, 'delete_new']);
        Route::post('/update',[LoaiSanPhamController::class, 'update']);
        Route::post('/edit',[LoaiSanPhamController::class, 'edit']);
    });
    Route::group(['prefix' => '/chuyen-muc'], function() {
        Route::get('/index-new', [ChuyenMucController::class, 'index']);
        Route::get('/data_new',[ChuyenMucController::class, 'data']);
        Route::post('/create_new',[ChuyenMucController::class, 'create']);
        Route::post('/delete_chuyenmuc',[ChuyenMucController::class, 'delete_chuyenmuc']);
    });
    Route::group(['prefix' => '/san-pham'], function() {
        Route::get('/index', [SanPhamController::class, 'index']);
        Route::get('/data',[SanPhamController::class, 'getDate']);

        Route::post('/create',[SanPhamController::class, 'create']);
        Route::post('/delete',[SanPhamController::class, 'delete']);
        Route::post('/update',[SanPhamController::class, 'update']);
        Route::post('/edit',[SanPhamController::class, 'edit']);
        Route::post('/status',[SanPhamController::class, 'status']);
    });
});
