@extends('admin_lte.shares.master_lte')
@section('content')
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">Quản Lý Chuyên Mục</h1>
                </div>
            </div>
        </div>
    </div>
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-5">
                    <form id="form_rs">
                        <div class="card">
                            <div class="card-header">
                                Thêm Mới Chuyên Mục
                            </div>
                            <div class="card-body">
                                <div class="form-group">
                                    <label>Tên Chuyên Mục</label>
                                    <input id="ten_chuyen_muc" type="text" class="form-control"
                                        placeholder="Nhập vào tên chuyên mục">
                                </div>
                                <div class="form-group">
                                    <label>Slug Chuyên Mục</label>
                                    <input id="slug_chuyen_muc" type="text" class="form-control"
                                        placeholder="Nhập vào slug chuyên mục">
                                </div>
                                <div class="form-group">
                                    <label>Tình Trạng</label>
                                    <select id="is_open" class="form-control">
                                        <option value="1">Hiển Thị</option>
                                        <option value="0">Tạm Tắt</option>
                                    </select>
                                </div>
                            </div>
                            <div class="card-footer text-right">
                                <button type="button" id="add" class="btn btn-primary">Thêm Mới</button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-md-7">
                    <div class="card">
                        <div class="card-header">
                            Danh Sách Chuyên Mục
                        </div>
                        <div class="card-body">
                            <table id="table" class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th class="text-center" scope="col">#</th>
                                        <th class="text-center" scope="col">Tên Chuyên Mục</th>
                                        <th class="text-center" scope="col">Slug Chuyên Mục</th>
                                        <th class="text-center" scope="col">Tình Trạng</th>
                                        <th class="text-center" scope="col">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                                <div class="modal fade" id="deleteModal" tabindex="-1" aria-labelledby="exampleModalLabel"
                                    aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">Xoá sản phẩm</h5>
                                                <button type="button" class="close" data-dismiss="modal"
                                                    aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <input type="text" class="form-control" id="delete_chuyen_muc">
                                                Bạn có chắc chắn muốn xóa loại sản phẩm này!
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary"
                                                    data-dismiss="modal">Close</button>
                                                <button type="button" id="delete_id" class="btn btn-primary" data-dismiss="modal">Save changes</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('js')
    <script>
        get();

        function show(list) {
            var content_table = '';
            $.each(list, function(key, value) {
                content_table += '<tr>';
                content_table += '<th scope="row" class="align-middle text-center">' + (key + 1) + '</th>';
                content_table += '<td class="align-middle text-center">' + value.ten_chuyen_muc + '</td>';
                content_table += '<td class="align-middle text-center">' + value.slug_chuyen_muc + '</td>';
                content_table += '<td class="align-middle text-center">';
                if (value.is_open) {
                    content_table += '<button type="button" class="btn btn-primary">Hiển Thị</button>';
                } else {
                    content_table += '<button type="button" class="btn btn-danger">Tạm Tắt</button>';
                }
                content_table += '</td>';
                content_table += '<th class="text-center align-middle">';
                content_table += '<button type="button" class="btn btn-info mr-1">Cập Nhật</button>'
                content_table +=
                    '<a id="delete_chuyen_muc" class="btn btn-danger" data-toggle="modal" data-target="#deleteModal">Xoá Bỏ</a>'
                content_table += '</th>';
                content_table += '</tr>';
            });

            $("#table tbody").html(content_table);
        }

        $("#add").click(function() {
            var chuyenmuc = {
                ten_chuyen_muc: $("#ten_chuyen_muc").val(),
                slug_chuyen_muc: $("#slug_chuyen_muc").val(),
                is_open: $("#is_open").val(),
            };

            axios
                .post('/admin-lte/chuyen-muc/create_new', chuyenmuc)
                .then((res) => {
                    if (res.data.status) {
                        toastr.success(res.data.messa);
                        $("#form_rs").trigger('reset');
                        get();
                    } else if (res.data.status == 0) {
                        toastr.error(res.data.messa);
                    } else if (res.data.status == 2) {
                        toastr.warning(res.data.messa);
                    }
                })
                .catch((res) => {
                    var ChuyenMuc = res.response.data.errors;
                    $.each(ChuyenMuc, function(k, v) {
                        toastr.error(v[0]);
                    });
                });
        })

        function get() {
            axios
                .get('/admin-lte/chuyen-muc/data_new')
                .then((res) => {
                    show(res.data.bede);
                })
        }

        $("#ten_chuyen_muc").keyup(function() {
            var noi_dung = $("#ten_chuyen_muc").val();
            var slug = toSlug(noi_dung);
            $("#slug_chuyen_muc").val(slug);
        });

        function toSlug(str) {
            str = str.toLowerCase();
            str = str
                .normalize('NFD')
                .replace(/[\u0300-\u036f]/g, '');
            str = str.replace(/[đĐ]/g, 'd');
            str = str.replace(/([^0-9a-z-\s])/g, '');
            str = str.replace(/(\s+)/g, '-');
            str = str.replace(/-+/g, '-');
            str = str.replace(/^-+|-+$/g, '');

            return str;
        }

        $("#delete_id").click(function(){
            var id_can_xoa = $("#delete_chuyen_muc").val();
            var id = {
                id : id_can_xoa,
            };
            axios
                .post('/admin-lte/chuyen-muc/delete_chuyenmuc', id)
                .then((res) => {
                    if (res.data.status == 1) {
                    } else if (res.data.status == 0) {
                        toastr.error(res.data.mess);
                    } else if (res.data.status == 2) {
                        toastr.warning(res.data.mess);
                    }
                    get();
                })
                .catch((res) => {
                    var ChuyenMuc = res.response.data.errors;
                    $.each(ChuyenMuc, function(k, v) {
                        toastr.error(v[0]);
                    });
                });
        })
    </script>
@endsection
